require 'pry'
require_relative 'input_reader.rb'
require_relative 'users_container.rb'
require_relative 'initialize_users.rb'
require_relative 'user_process.rb'

module Files
  INPUT = 'input.csv'
  USER_DATA= 'user_data.csv'
end
  
  puts "Enter input file name: "
	input_file_name = $stdin.read

	user_data = InputReader.new(Files::USER_DATA)
  input_reader = InputReader.new(input_file_name = Files::INPUT)

  initialize_user = InitializeUser.new(user_data)
  initialize_user.populate_user
  
  user_processor = UserProcessor.new(input_reader)
  user_processor.process_user
