require 'csv'
require 'pry'
require_relative 'file_checker.rb'
class InputReader

  def initialize(input_file = INPUT_FILE_NAME)
    @input_file_name  = input_file
    if(FileChecker.check_existance(@input_file_name) and FileChecker.check_read_permission(@input_file_name))
      puts "your file found and it is readable!!!"
    else
      raise "No such file #{@input_file_name}"
    end
  end

  def read_file
    row_counter=0
    array_of_file =Array.new
    CSV.foreach(@input_file_name, converters: :numeric) do |row|
      if(row_counter>=1)
        array_of_file << row
        #puts row.inspect
      end
      row_counter = row_counter +1
    end
    return array_of_file
  end
end