require 'singleton'
class UsersContainer
  include Singleton
  def initialize
    @user_array ||= []
  end

  def add (user_object)
    @user_array << user_object
  end

  def get_user_array()
    return @user_array    
  end
end