require 'pry'
require_relative 'user.rb'
class InputUser
  def initialize(name,password,action)
    @name = name
    @password = password
    @action = action.chomp.split(",")
    @status_name = false
    @status_password = false
    #@status_action = false
  end
  def match_status
    status_action = Array.new
    user_data = UsersContainer.instance.get_user_array
    user_data.each do |user|
      if (user.get_name == @name)
          @status_name = true
          if(user.get_password == @password)
            @status_password = true        
            user.get_action_permitted.each do |action|
              @action.each do |input_action|
                if(input_action == action)
                  status_action << action
                end
              end
            end
          end
      end 
      
    end
    return @status_name, @status_password, status_action
  end
end