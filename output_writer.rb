require 'csv'
require 'pry'

class OutputWriter

  def self.write_to_console(name,status_name,status_password,status_action)
    if status_name
      if status_password
        if status_action.length != 0
          print "#{name} has "
          self.print_actions(status_action)
          print "action permitted!\n"
        else
          puts "#{name} has no matched action permitted"
        end
      else
        puts "#{name} has wrong password!!"
      end
    else
      puts "#{name} is not present in users list!!!"
    end 
  end
  
  def self.print_actions(status_action)
    status_action.each do |action|
      print action, " "
    end
  end
end