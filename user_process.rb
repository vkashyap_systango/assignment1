require 'pry'
require_relative 'input_user.rb'
require_relative 'output_writer.rb'
class UserProcessor

  def initialize(input_file)
    @input_reader = input_file
  end

  def process_user
    file = @input_reader.read_file
    file.each do |row|
      input_user_object = InputUser.new((get_name(row)),(get_password(row)),(get_actions(row)))
      status_name, status_password, status_action = input_user_object.match_status
      #binding.pry
      OutputWriter.write_to_console(get_name(row),status_name,status_password,status_action)
    end
  end

  def get_name(row)
    return row[0].to_s rescue ''
  end
  def get_password(row)
    return row[1].to_s rescue ''
  end
  def get_actions(row)
    return row[2].to_s rescue ''
  end
  private :get_name, :get_password, :get_actions
end