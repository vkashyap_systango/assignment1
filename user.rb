class User
  def initialize(name,password,action_permited)
    @name = name
    @password =password
    @action_permited = action_permited.chomp.split(",")
  end

  def get_name
    return @name    
  end
  def get_password
    return @password
  end
  def get_action_permitted
    return @action_permited
  end
end