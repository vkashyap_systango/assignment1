require 'pry'
require_relative 'user.rb'
class InitializeUser
  def initialize(user_data_csv)
    @input_reader = user_data_csv
  end

  def populate_user 
    file = @input_reader.read_file
    file.each do |row|
      user_object = User.new((get_name(row)),(get_password(row)),(get_actions_permitted(row)))
      UsersContainer.instance.add(user_object)
     # binding.pry
    end
  end

  def get_name(row)
    return row[0].to_s rescue ''
  end
  def get_password(row)
    return row[1].to_s rescue ''
  end
  def get_actions_permitted(row)
    return row[2].to_s rescue ''
  end

  private :get_name, :get_password, :get_actions_permitted

end